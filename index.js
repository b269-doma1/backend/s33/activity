


fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => console.log(data))



fetch('https://jsonplaceholder.typicode.com/todos')
.then(response => response.json())
.then(data => {
    const titles = data.map(item => item.title);
    console.log(titles);
});




fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: 'GET',
  headers: {
    'Content-Type': 'application/json'
  }
})

.then(response => response.json())
.then(data => { console.log(data)});
 


fetch('https://jsonplaceholder.typicode.com/todos/', {
  method: 'POST',
  headers:  {
    'Content-Type': 'application/json'
        },
  body: JSON.stringify({
    title: "Created to do list",
    completed: false,
    userId: 1
  })
})
  .then(response => response.json())
  .then(data => console.log(data));


fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PUT',
  headers: {
    'Content-type': 'application/json; charset=UTF-8',
  },
  body: JSON.stringify({
    dateCompleted: 'Pending',
    description: 'To update the my to do list with a different data instructor',
    id: 1,
    status: 'Pending',
    title: 'Updated to do list item',
    userId: 1
    
  })
})
  .then(response => response.json())
  .then(data => console.log(data));



fetch('https://jsonplaceholder.typicode.com/todos/1', {
  method: 'PATCH',
  headers: {
    'Content-type': 'application/json; charset=UTF-8',
    },
  body: JSON.stringify({
    completed: false,
    dateCompleted: "07/09/21",
    id: 1,
    status: "Complete"

  }) 
})
  .then(response => response.json())
  .then(data => console.log(data));


fetch("https://jsonplaceholder.typicode.com/todos/1", {
  method: 'DELETE'
});


  